export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCFX7u09mjjvJ2tNDxhYY4TPcpWIT6bnxw",
    authDomain: "sweltering-torch-5096.firebaseapp.com",
    databaseURL: "https://sweltering-torch-5096.firebaseio.com",
    projectId: "sweltering-torch-5096",
    storageBucket: "sweltering-torch-5096.appspot.com",
    messagingSenderId: "286812105783",
    appId: "1:286812105783:web:7161deb14e7481e401a3b2"
  }
};
