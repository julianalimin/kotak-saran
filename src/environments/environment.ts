// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCFX7u09mjjvJ2tNDxhYY4TPcpWIT6bnxw",
    authDomain: "sweltering-torch-5096.firebaseapp.com",
    databaseURL: "https://sweltering-torch-5096.firebaseio.com",
    projectId: "sweltering-torch-5096",
    storageBucket: "sweltering-torch-5096.appspot.com",
    messagingSenderId: "286812105783",
    appId: "1:286812105783:web:7161deb14e7481e401a3b2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
