import { Component } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection
} from "@angular/fire/firestore";
import { Observable } from "rxjs/internal/Observable";
import { AstMemoryEfficientTransformer } from "@angular/compiler";

export interface Item {
  name: string;
}

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage {
  private daftarIde: AngularFirestoreCollection<Item>;
  items: Observable<Item[]>;
  ide: any;

  constructor(private afs: AngularFirestore) {
    this.daftarIde = afs.collection<Item>("daftarIde");
    this.items = this.daftarIde.valueChanges();
  }
  tambahIde() {
    this.daftarIde.add({
      name: this.ide
    });
  }
}
